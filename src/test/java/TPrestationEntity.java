import javax.persistence.*;

@Entity
@Table(name = "T_PRESTATION", schema = "dbo", catalog = "PROGICA")
public class TPrestationEntity
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */
    private int pstId;
    private String pstLibelle;

    @Id
    @Column(name = "PST_ID")
    public int getPstId()
    {
        return pstId;
    }

    public void setPstId(int pstId)
    {
        this.pstId = pstId;
    }

    @Basic
    @Column(name = "PST_LIBELLE")
    public String getPstLibelle()
    {
        return pstLibelle;
    }

    public void setPstLibelle(String pstLibelle)
    {
        this.pstLibelle = pstLibelle;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TPrestationEntity that = (TPrestationEntity) o;

        if (pstId != that.pstId)
        {
            return false;
        }
        if (pstLibelle != null ? !pstLibelle.equals(that.pstLibelle) : that.pstLibelle != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = pstId;
        result = 31 * result + (pstLibelle != null ? pstLibelle.hashCode() : 0);
        return result;
    }
}
