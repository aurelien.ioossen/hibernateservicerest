import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class TEtreDisponibleEntityPK implements Serializable
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */
    private int edpPrsId;
    private int edpDspId;

    @Column(name = "EDP_PRS_ID")
    @Id
    public int getEdpPrsId()
    {
        return edpPrsId;
    }

    public void setEdpPrsId(int edpPrsId)
    {
        this.edpPrsId = edpPrsId;
    }

    @Column(name = "EDP_DSP_ID")
    @Id
    public int getEdpDspId()
    {
        return edpDspId;
    }

    public void setEdpDspId(int edpDspId)
    {
        this.edpDspId = edpDspId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TEtreDisponibleEntityPK that = (TEtreDisponibleEntityPK) o;

        if (edpPrsId != that.edpPrsId)
        {
            return false;
        }
        if (edpDspId != that.edpDspId)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = edpPrsId;
        result = 31 * result + edpDspId;
        return result;
    }
}
