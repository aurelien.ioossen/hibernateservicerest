import javax.persistence.*;

@Entity
@Table(name = "T_DISPONIBILITE", schema = "dbo", catalog = "PROGICA")
public class TDisponibiliteEntity
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */
    private int dptId;
    private String dptJourDebut;
    private String dptJourFin;
    private String dptHeureDebut;
    private String dptHeureFin;

    @Id
    @Column(name = "DPT_ID")
    public int getDptId()
    {
        return dptId;
    }

    public void setDptId(int dptId)
    {
        this.dptId = dptId;
    }

    @Basic
    @Column(name = "DPT_JOUR_DEBUT")
    public String getDptJourDebut()
    {
        return dptJourDebut;
    }

    public void setDptJourDebut(String dptJourDebut)
    {
        this.dptJourDebut = dptJourDebut;
    }

    @Basic
    @Column(name = "DPT_JOUR_FIN")
    public String getDptJourFin()
    {
        return dptJourFin;
    }

    public void setDptJourFin(String dptJourFin)
    {
        this.dptJourFin = dptJourFin;
    }

    @Basic
    @Column(name = "DPT_HEURE_DEBUT")
    public String getDptHeureDebut()
    {
        return dptHeureDebut;
    }

    public void setDptHeureDebut(String dptHeureDebut)
    {
        this.dptHeureDebut = dptHeureDebut;
    }

    @Basic
    @Column(name = "DPT_HEURE_FIN")
    public String getDptHeureFin()
    {
        return dptHeureFin;
    }

    public void setDptHeureFin(String dptHeureFin)
    {
        this.dptHeureFin = dptHeureFin;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TDisponibiliteEntity that = (TDisponibiliteEntity) o;

        if (dptId != that.dptId)
        {
            return false;
        }
        if (dptJourDebut != null ? !dptJourDebut.equals(that.dptJourDebut) : that.dptJourDebut != null)
        {
            return false;
        }
        if (dptJourFin != null ? !dptJourFin.equals(that.dptJourFin) : that.dptJourFin != null)
        {
            return false;
        }
        if (dptHeureDebut != null ? !dptHeureDebut.equals(that.dptHeureDebut) : that.dptHeureDebut != null)
        {
            return false;
        }
        if (dptHeureFin != null ? !dptHeureFin.equals(that.dptHeureFin) : that.dptHeureFin != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = dptId;
        result = 31 * result + (dptJourDebut != null ? dptJourDebut.hashCode() : 0);
        result = 31 * result + (dptJourFin != null ? dptJourFin.hashCode() : 0);
        result = 31 * result + (dptHeureDebut != null ? dptHeureDebut.hashCode() : 0);
        result = 31 * result + (dptHeureFin != null ? dptHeureFin.hashCode() : 0);
        return result;
    }
}
