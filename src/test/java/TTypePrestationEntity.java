import javax.persistence.*;

@Entity
@Table(name = "T_TYPE_PRESTATION", schema = "dbo", catalog = "PROGICA")
public class TTypePrestationEntity
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */
    private int typeId;
    private String typeLibelle;

    @Id
    @Column(name = "TYPE_ID")
    public int getTypeId()
    {
        return typeId;
    }

    public void setTypeId(int typeId)
    {
        this.typeId = typeId;
    }

    @Basic
    @Column(name = "TYPE_LIBELLE")
    public String getTypeLibelle()
    {
        return typeLibelle;
    }

    public void setTypeLibelle(String typeLibelle)
    {
        this.typeLibelle = typeLibelle;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TTypePrestationEntity that = (TTypePrestationEntity) o;

        if (typeId != that.typeId)
        {
            return false;
        }
        if (typeLibelle != null ? !typeLibelle.equals(that.typeLibelle) : that.typeLibelle != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = typeId;
        result = 31 * result + (typeLibelle != null ? typeLibelle.hashCode() : 0);
        return result;
    }
}
