import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "T_PERIODE", schema = "dbo", catalog = "PROGICA")
public class TPeriodeEntity
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */
    private int prdId;
    private Date prdDateDebut;
    private Date prdDateFin;

    @Id
    @Column(name = "PRD_ID")
    public int getPrdId()
    {
        return prdId;
    }

    public void setPrdId(int prdId)
    {
        this.prdId = prdId;
    }

    @Basic
    @Column(name = "PRD_DATE_DEBUT")
    public Date getPrdDateDebut()
    {
        return prdDateDebut;
    }

    public void setPrdDateDebut(Date prdDateDebut)
    {
        this.prdDateDebut = prdDateDebut;
    }

    @Basic
    @Column(name = "PRD_DATE_FIN")
    public Date getPrdDateFin()
    {
        return prdDateFin;
    }

    public void setPrdDateFin(Date prdDateFin)
    {
        this.prdDateFin = prdDateFin;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TPeriodeEntity that = (TPeriodeEntity) o;

        if (prdId != that.prdId)
        {
            return false;
        }
        if (prdDateDebut != null ? !prdDateDebut.equals(that.prdDateDebut) : that.prdDateDebut != null)
        {
            return false;
        }
        if (prdDateFin != null ? !prdDateFin.equals(that.prdDateFin) : that.prdDateFin != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = prdId;
        result = 31 * result + (prdDateDebut != null ? prdDateDebut.hashCode() : 0);
        result = 31 * result + (prdDateFin != null ? prdDateFin.hashCode() : 0);
        return result;
    }
}
