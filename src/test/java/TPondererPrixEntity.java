import javax.persistence.*;

@Entity
@Table(name = "T_PONDERER_PRIX", schema = "dbo", catalog = "PROGICA")
@IdClass(TPondererPrixEntityPK.class)
public class TPondererPrixEntity
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */
    private int pdpIdPeriode;
    private int pdpIdGite;
    private Double pdpTaux;

    @Id
    @Column(name = "PDP_ID_PERIODE")
    public int getPdpIdPeriode()
    {
        return pdpIdPeriode;
    }

    public void setPdpIdPeriode(int pdpIdPeriode)
    {
        this.pdpIdPeriode = pdpIdPeriode;
    }

    @Id
    @Column(name = "PDP_ID_GITE")
    public int getPdpIdGite()
    {
        return pdpIdGite;
    }

    public void setPdpIdGite(int pdpIdGite)
    {
        this.pdpIdGite = pdpIdGite;
    }

    @Basic
    @Column(name = "PDP_TAUX")
    public Double getPdpTaux()
    {
        return pdpTaux;
    }

    public void setPdpTaux(Double pdpTaux)
    {
        this.pdpTaux = pdpTaux;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TPondererPrixEntity that = (TPondererPrixEntity) o;

        if (pdpIdPeriode != that.pdpIdPeriode)
        {
            return false;
        }
        if (pdpIdGite != that.pdpIdGite)
        {
            return false;
        }
        if (pdpTaux != null ? !pdpTaux.equals(that.pdpTaux) : that.pdpTaux != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = pdpIdPeriode;
        result = 31 * result + pdpIdGite;
        result = 31 * result + (pdpTaux != null ? pdpTaux.hashCode() : 0);
        return result;
    }
}
