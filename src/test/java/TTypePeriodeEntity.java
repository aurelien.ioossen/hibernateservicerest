import javax.persistence.*;

@Entity
@Table(name = "T_TYPE_PERIODE", schema = "dbo", catalog = "PROGICA")
public class TTypePeriodeEntity
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */
    private int tprId;
    private String tprLibelle;

    @Id
    @Column(name = "TPR_ID")
    public int getTprId()
    {
        return tprId;
    }

    public void setTprId(int tprId)
    {
        this.tprId = tprId;
    }

    @Basic
    @Column(name = "TPR_LIBELLE")
    public String getTprLibelle()
    {
        return tprLibelle;
    }

    public void setTprLibelle(String tprLibelle)
    {
        this.tprLibelle = tprLibelle;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TTypePeriodeEntity that = (TTypePeriodeEntity) o;

        if (tprId != that.tprId)
        {
            return false;
        }
        if (tprLibelle != null ? !tprLibelle.equals(that.tprLibelle) : that.tprLibelle != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = tprId;
        result = 31 * result + (tprLibelle != null ? tprLibelle.hashCode() : 0);
        return result;
    }
}
