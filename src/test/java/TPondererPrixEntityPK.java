import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class TPondererPrixEntityPK implements Serializable
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */
    private int pdpIdPeriode;
    private int pdpIdGite;

    @Column(name = "PDP_ID_PERIODE")
    @Id
    public int getPdpIdPeriode()
    {
        return pdpIdPeriode;
    }

    public void setPdpIdPeriode(int pdpIdPeriode)
    {
        this.pdpIdPeriode = pdpIdPeriode;
    }

    @Column(name = "PDP_ID_GITE")
    @Id
    public int getPdpIdGite()
    {
        return pdpIdGite;
    }

    public void setPdpIdGite(int pdpIdGite)
    {
        this.pdpIdGite = pdpIdGite;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TPondererPrixEntityPK that = (TPondererPrixEntityPK) o;

        if (pdpIdPeriode != that.pdpIdPeriode)
        {
            return false;
        }
        if (pdpIdGite != that.pdpIdGite)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = pdpIdPeriode;
        result = 31 * result + pdpIdGite;
        return result;
    }
}
