import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class TProposerPrestationEntityPK implements Serializable
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */
    private int prpGiteId;
    private int prpPrestationId;

    @Column(name = "PRP_GITE_ID")
    @Id
    public int getPrpGiteId()
    {
        return prpGiteId;
    }

    public void setPrpGiteId(int prpGiteId)
    {
        this.prpGiteId = prpGiteId;
    }

    @Column(name = "PRP_PRESTATION_ID")
    @Id
    public int getPrpPrestationId()
    {
        return prpPrestationId;
    }

    public void setPrpPrestationId(int prpPrestationId)
    {
        this.prpPrestationId = prpPrestationId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TProposerPrestationEntityPK that = (TProposerPrestationEntityPK) o;

        if (prpGiteId != that.prpGiteId)
        {
            return false;
        }
        if (prpPrestationId != that.prpPrestationId)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = prpGiteId;
        result = 31 * result + prpPrestationId;
        return result;
    }
}
