import javax.persistence.*;

@Entity
@Table(name = "T_SOUS_TYPE_PRESTATION", schema = "dbo", catalog = "PROGICA")
public class TSousTypePrestationEntity
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */
    private int sstId;
    private String sstLibelle;

    @Id
    @Column(name = "SST_ID")
    public int getSstId()
    {
        return sstId;
    }

    public void setSstId(int sstId)
    {
        this.sstId = sstId;
    }

    @Basic
    @Column(name = "SST_LIBELLE")
    public String getSstLibelle()
    {
        return sstLibelle;
    }

    public void setSstLibelle(String sstLibelle)
    {
        this.sstLibelle = sstLibelle;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TSousTypePrestationEntity that = (TSousTypePrestationEntity) o;

        if (sstId != that.sstId)
        {
            return false;
        }
        if (sstLibelle != null ? !sstLibelle.equals(that.sstLibelle) : that.sstLibelle != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = sstId;
        result = 31 * result + (sstLibelle != null ? sstLibelle.hashCode() : 0);
        return result;
    }
}
