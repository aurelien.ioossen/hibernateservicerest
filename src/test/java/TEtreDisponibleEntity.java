import javax.persistence.*;

@Entity
@Table(name = "T_ETRE_DISPONIBLE", schema = "dbo", catalog = "PROGICA")
@IdClass(TEtreDisponibleEntityPK.class)
public class TEtreDisponibleEntity
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */
    private int edpPrsId;
    private int edpDspId;

    @Id
    @Column(name = "EDP_PRS_ID")
    public int getEdpPrsId()
    {
        return edpPrsId;
    }

    public void setEdpPrsId(int edpPrsId)
    {
        this.edpPrsId = edpPrsId;
    }

    @Id
    @Column(name = "EDP_DSP_ID")
    public int getEdpDspId()
    {
        return edpDspId;
    }

    public void setEdpDspId(int edpDspId)
    {
        this.edpDspId = edpDspId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TEtreDisponibleEntity that = (TEtreDisponibleEntity) o;

        if (edpPrsId != that.edpPrsId)
        {
            return false;
        }
        if (edpDspId != that.edpDspId)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = edpPrsId;
        result = 31 * result + edpDspId;
        return result;
    }
}
