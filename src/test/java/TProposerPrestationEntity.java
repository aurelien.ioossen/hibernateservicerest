import javax.persistence.*;

@Entity
@Table(name = "T_PROPOSER_PRESTATION", schema = "dbo", catalog = "PROGICA")
@IdClass(TProposerPrestationEntityPK.class)
public class TProposerPrestationEntity
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */
    private Integer prpPrix;
    private int prpGiteId;
    private int prpPrestationId;

    @Basic
    @Column(name = "PRP_PRIX")
    public Integer getPrpPrix()
    {
        return prpPrix;
    }

    public void setPrpPrix(Integer prpPrix)
    {
        this.prpPrix = prpPrix;
    }

    @Id
    @Column(name = "PRP_GITE_ID")
    public int getPrpGiteId()
    {
        return prpGiteId;
    }

    public void setPrpGiteId(int prpGiteId)
    {
        this.prpGiteId = prpGiteId;
    }

    @Id
    @Column(name = "PRP_PRESTATION_ID")
    public int getPrpPrestationId()
    {
        return prpPrestationId;
    }

    public void setPrpPrestationId(int prpPrestationId)
    {
        this.prpPrestationId = prpPrestationId;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TProposerPrestationEntity that = (TProposerPrestationEntity) o;

        if (prpGiteId != that.prpGiteId)
        {
            return false;
        }
        if (prpPrestationId != that.prpPrestationId)
        {
            return false;
        }
        if (prpPrix != null ? !prpPrix.equals(that.prpPrix) : that.prpPrix != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = prpPrix != null ? prpPrix.hashCode() : 0;
        result = 31 * result + prpGiteId;
        result = 31 * result + prpPrestationId;
        return result;
    }
}
