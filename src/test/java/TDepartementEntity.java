import javax.persistence.*;

@Entity
@Table(name = "T_DEPARTEMENT", schema = "dbo", catalog = "PROGICA")
public class TDepartementEntity
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */
    private String dptNum;
    private String dptNom;

    @Id
    @Column(name = "DPT_NUM")
    public String getDptNum()
    {
        return dptNum;
    }

    public void setDptNum(String dptNum)
    {
        this.dptNum = dptNum;
    }

    @Basic
    @Column(name = "DPT_NOM")
    public String getDptNom()
    {
        return dptNom;
    }

    public void setDptNom(String dptNom)
    {
        this.dptNom = dptNom;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TDepartementEntity that = (TDepartementEntity) o;

        if (dptNum != null ? !dptNum.equals(that.dptNum) : that.dptNum != null)
        {
            return false;
        }
        if (dptNom != null ? !dptNom.equals(that.dptNom) : that.dptNom != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = dptNum != null ? dptNum.hashCode() : 0;
        result = 31 * result + (dptNom != null ? dptNom.hashCode() : 0);
        return result;
    }
}
