package rest;

import dao.hibernate.GenericDAO;
import job.Departement;
import job.Region;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.hibernate.criterion.Restrictions;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author IOOSSEN AURELIEN created on 29/10/2019
 */

@Path("/regions")
@Tag(name = "Régions")
@Produces(MediaType.APPLICATION_JSON)

public class RegionRest
{


    @GET
    @Operation(summary = "retourne la liste des Régions")
    public Response getRegions()
    {
        GenericDAO<Region, String> regionDAO = new GenericDAO(Region.class);
        List listRegion = regionDAO.getAll();
        if (listRegion == null)
        {
            return Response.noContent().build();
        }
        else
        {
            return Response.ok(new GenericEntity<List<Region>>(listRegion)
            {
            }).build();
        }
    }


    @GET
    @Operation(summary = "retourne la Région pour l'ID concerné")
    @Path("{id}")

    public Response getRegionByID(@PathParam("id") String code_region)
    {
        GenericDAO<Region, String> regionDAO = new GenericDAO(Region.class);
        Region regionWanted = (Region) regionDAO.getByid(code_region);
        return Response.ok(new GenericEntity<>(regionWanted)
        {
        }).build();

    }

    @GET
    @Operation(summary = "Retourne les regions dont le nom contient la chaine de caractère ")
    @Path("/like")
    public Response getRegionLike(@QueryParam("contient") String chaineWanted)
    {

        GenericDAO<Region, String> regionDAO = new GenericDAO(Region.class);
        List listRegion = regionDAO.getByCriteria(Restrictions.like("RGN_nom", "%"+chaineWanted+"%"));
        return Response.ok(new GenericEntity<>(listRegion)
        {
        }).build();

    }


    @GET
    @Operation(summary = "retourne la liste des departements de la région")
    @Path("{id}/departements")
    public Response getDepartementsOfRegion(@PathParam("id") String idRegion){

        GenericDAO<Region, String> regionDAO = new GenericDAO(Region.class);
        Region region = (Region) regionDAO.getByid(idRegion);
        System.out.println(region.toString());
        List<Departement> departementList = region.getListDepartement();

        if (departementList != null){
            return Response.ok(new GenericEntity<List<Departement>>(departementList){}).build();
        }else{
            return Response.noContent().build();
        }

    }


}
