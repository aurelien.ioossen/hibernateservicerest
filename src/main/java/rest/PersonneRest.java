package rest;

import dao.hibernate.GenericDAO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import job.Personne;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author IOOSSEN AURELIEN created on 06/11/2019
 */

@Path("/personnes")
@Tag(name = "Personnes")
@Produces(MediaType.APPLICATION_JSON)


public class PersonneRest
{
    GenericDAO<Personne, Integer> personneDao;

    public PersonneRest()
    {
        personneDao = new GenericDAO<>(Personne.class);
    }

    @GET
    @Operation(summary = "Retourne la liste des personnes")
    public Response getPersonnes()
    {

        List personneList = personneDao.getAll();
        if (personneList != null)
        {

            return Response.ok(new GenericEntity<List<Personne>>(personneList)
            {
            }).build();


        }
        else
        {
            return Response.noContent().build();

        }


    }


    @GET
    @Path("{id}")
    @Operation(summary = "Retourn la personne corrrespondant à l'ID")
    public Response getPersonneById(@PathParam("id") Integer idPersonne)
    {
        Personne personne = (Personne) personneDao.getByid(idPersonne);
        if (personne != null)
        {

            return Response.ok(new GenericEntity<Personne>(personne)
            {
            }).build();
        }
        else
        {
            return Response.noContent().build();
        }
    }


    @DELETE
    @Path("{id}")
    public void deletePersonne(@PathParam("id") Integer idPersonne)
    {
        Personne personne = new Personne();
        personne.setId_personne(idPersonne);
        System.out.println("Suppression : "+personne.toString());
        personneDao.delete(personne) ;

    }
}
