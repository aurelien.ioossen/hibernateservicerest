package rest;

import dao.hibernate.GenericDAO;
import dao.hibernate.GiteDAO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import job.Gite;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/gites")
@Tag(name = "Gites")
@Produces(MediaType.APPLICATION_JSON)


public class GiteRest
{


    /**
     * @author IOOSSEN AURELIEN created on 29/10/2019
     */

    GenericDAO<Gite, Integer> giteDAO = new GenericDAO(Gite.class);
    private Logger log = LogManager.getLogger("warn.log");

    @GET
    @Operation(summary = "Retourne la liste eds Gites")
    public Response getGites()
    {

        List listGites = giteDAO.getAll();
        if (listGites == null)
        {
            return Response.noContent().build();
        }

        return Response.ok(new GenericEntity<List<Gite>>(listGites)
        {
        }).build();


    }


    @GET
    @Operation(summary = "retourne le gite correspondant à l'id")
    @Path("{idGite}")
    public Response getGiteById(@PathParam("idGite") Integer idGite)
    {
        GenericDAO<Gite, Integer> giteDAO = new GenericDAO(Gite.class);
        Gite giteEntity = (Gite) giteDAO.getByid(idGite);
        if (giteEntity == null)
        {
            return Response.noContent().build();
        }
        else
        {
            log.info("getByid en cours");
            return Response.ok(new GenericEntity<>(giteEntity){}).build();

        }

    }

    @DELETE
    @Operation(summary = "Suppression du gite correspondant")
    @Path("{idGite}")
    public Response deleteGiteById(@PathParam("idGite") Integer idGite)
    {
        GenericDAO<Gite, Integer> giteDAO = new GenericDAO(Gite.class);
        if (giteDAO.delete((Gite) giteDAO.getByid(idGite)))
        {

            return Response.ok().build();

        }
        else
        {
            return Response.serverError().build();
        }

    }

    @POST
    @Operation(summary = "retourne liste des Gites contenant la recherche dans le LIBELLE")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGiteQBE(Gite giteSearched)
    {

        GiteDAO giteDAO = new GiteDAO(Gite.class);
        List<Gite> listGite = giteDAO.getGiteQBE(giteSearched);
        if (listGite != null)
        {
            return Response.ok(new GenericEntity<List<Gite>>(listGite)
            {
            }).build();

        }
        else
        {
            return Response.noContent().build();
        }

    }


}
