package dao.hibernate;

import org.hibernate.criterion.Criterion;

import java.io.Serializable;
import java.util.List;

public abstract interface IGenericDAO<T, S extends Serializable>
{

    /**
     * @author IOOSSEN AURELIEN created on 04/11/2019
     */


    T getByid(S id) throws DAOException;


    List<T> getAll() throws DAOException;


    List<T> getByCriteria(Criterion... criterionlist);

    boolean delete(T objetAsupprimer) throws DAOException ;

}
