package dao.hibernate;

import job.Gite;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class GiteDAO extends GenericDAO
{
    /**
     * @author IOOSSEN AURELIEN created on 06/11/2019
     */

    public GiteDAO(Class persistentClass)
    {
        super(persistentClass);
    }


    public List<Gite> getGiteQBE(Gite giteSearched)
    {

        Session session = HibernateUtils.buildSessionFactory().getCurrentSession();
//        session.beginTransaction();
//        Query query = session.createQuery("from Gite where libelleGite like :libelleSearched");
//        query.setParameter("libelleSearched", "%"+giteSearched.getLibelleGite()+"%");
//        List listGite = query.getResultList();
//        session.getTransaction().commit();
//        return listGite;

        Criteria crit = session.createCriteria(Gite.class);
        crit.add(Restrictions.like("libelleGite", "%" + giteSearched.getLibelleGite() + "%"));
        List listGite = crit.list();
        return listGite;


    }


}


