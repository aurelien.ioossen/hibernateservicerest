package dao.hibernate;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import java.io.Serializable;
import java.util.List;

public class GenericDAO<T, S extends Serializable> implements IGenericDAO<T,S>
{

    /**
     * @author IOOSSEN AURELIEN created on 04/11/2019
     */

    private Session session;
    private Class<T> persistentClass;

    public GenericDAO(Class<T> persistentClass)
    {
        this.persistentClass = persistentClass;
        session = HibernateUtils.buildSessionFactory().getCurrentSession();
    }


    @Override
    public Object getByid(Serializable id)
    {

        System.out.println("getByid !");
        T entity;
        session.beginTransaction();
        entity = (T) session.get(persistentClass, (Serializable) id);
        session.getTransaction().commit();
        return entity;
    }

    public List<T> getAll()
    {

        return getByCriteria();

    }


    public List<T> getByCriteria(Criterion... criterionlist)
    {
        session.beginTransaction();
        Criteria criteria = session.createCriteria(persistentClass);

        for (Criterion criterion : criterionlist)
        {
            criteria.add(criterion);

        }
        List resultList = criteria.list();
        session.getTransaction().commit();
        return resultList;
    }




    @Override
    public boolean delete(T objetAsupprimer)
    {
        boolean isDeleted = false;

        try
        {
            session.beginTransaction();
            session.delete(objetAsupprimer);
            session.getTransaction().commit();
            isDeleted = true;
        }
        catch (Exception e)
        {
            System.out.println("Suppression impossible");
            System.out.println(e);

        }
        return isDeleted;
    }


}
