package dao.hibernate;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.Logger;

public class DAOException extends RuntimeException
{

    /**
     * @author IOOSSEN AURELIEN created on 04/11/2019
     */

    private  final Logger logger = (Logger) LogManager.getLogger("warn.log");
    private String message;

    public DAOException(String msg)
    {
        super(msg);
        this.message = msg;
        logger.error(msg);

    }

    @Override
    public String getMessage()
    {
        return message;
    }

    public  Logger getLogger()
    {
        return logger;
    }
}
