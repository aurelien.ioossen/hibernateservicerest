package job;

import java.util.ArrayList;

public class Sous_type_Prestation
{

    /**
     * @author IOOSSEN AURELIEN created on 27/08/2019
     */

    private int id_sous_type;
    private String libelle_sous_type;
    private Type_prestation type_prestation;
    private ArrayList<Prestation> listePrestation;


    public Sous_type_Prestation(int id_sous_type, String libelle_sous_type)
    {
        this.id_sous_type = id_sous_type;
        this.libelle_sous_type = libelle_sous_type;
    }


    public int getId_sous_type()
    {
        return id_sous_type;
    }

    public void setId_sous_type(int id_sous_type)
    {
        this.id_sous_type = id_sous_type;
    }

    public String getLibelle_sous_type()
    {
        return libelle_sous_type;
    }

    public void setLibelle_sous_type(String libelle_sous_type)
    {
        this.libelle_sous_type = libelle_sous_type;
    }

    public Type_prestation getType_prestation()
    {
        return type_prestation;
    }

    public void setType_prestation(Type_prestation type_prestation)
    {
        this.type_prestation = type_prestation;
    }

    public ArrayList<Prestation> getListePrestation()
    {
        return listePrestation;
    }

    public void setListePrestation(ArrayList<Prestation> listePrestation)
    {
        this.listePrestation = listePrestation;
    }


}
