package job;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Objects;

/**
 * @author IOOSSEN AURELIEN created on 27/08/2019
 */

@Entity
@Table(name = "T_DEPARTEMENT",schema = "dbo", catalog = "PROGICA")
public class Departement
{
    @Id
    @Column(name = "DPT_NUM")
    private String numeroDepartement;
    @Column(name = "DPT_NOM")
    private String nomDepartement;
    @Transient
    private Region region;
    @Transient
    private ArrayList<Ville> listeVilles;


    public Departement(String numero_departement, String nom_departement)
    {
        this.numeroDepartement = numero_departement;
        this.nomDepartement = nom_departement;
        listeVilles = new ArrayList<>();
    }

    public Departement()
    {
    }

    public String getNumeroDepartement()
    {
        return numeroDepartement;
    }

    public void setNumeroDepartement(String numeroDepartement)
    {
        this.numeroDepartement = numeroDepartement;
    }

    public String getNomDepartement()
    {
        return nomDepartement;
    }

    public void setNomDepartement(String nomDepartement)
    {
        this.nomDepartement = nomDepartement;
    }

    public Region getRegion()
    {
        return region;
    }

    public void setRegion(Region region)
    {
        this.region = region;
    }

    public ArrayList<Ville> getListOfCityOfDepartment()
    {
        return listeVilles;
    }

    public void setListeVilles(ArrayList<Ville> listeVilles)
    {
        this.listeVilles = listeVilles;
    }

    public void addVille(Ville v)
    {

        listeVilles.add(v);
    }

    public void removeVille(Ville v)
    {
        listeVilles.remove(v);
    }


    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Departement that = (Departement) o;
        return Objects.equals(numeroDepartement, that.numeroDepartement) && Objects.equals(nomDepartement, that.nomDepartement) && Objects.equals(region, that.region) && Objects.equals(listeVilles, that.listeVilles);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(numeroDepartement, nomDepartement, region, listeVilles);
    }

    @Override
    public String toString()
    {
        if (getNumeroDepartement().equals(""))
        {
            return nomDepartement;
        }
        else

        return numeroDepartement +" - "+ nomDepartement;
}
}
