package job;

import java.util.ArrayList;

public class Type_prestation
{

    /**
     * @author IOOSSEN AURELIEN created on 27/08/2019
     */

    private int id_type;
    private String libelle_type;
    private ArrayList<Sous_type_Prestation> listePSousType;


    public Type_prestation(int id_type, String libelle_type)
    {
        this.id_type = id_type;
        this.libelle_type = libelle_type;
    }

    public int getId_type()
    {
        return id_type;
    }

    public void setId_type(int id_type)
    {
        this.id_type = id_type;
    }

    public String getLibelle_type()
    {
        return libelle_type;
    }

    public void setLibelle_type(String libelle_type)
    {
        this.libelle_type = libelle_type;
    }

    public ArrayList<Sous_type_Prestation> getListePSousType()
    {
        return listePSousType;
    }

    public void setListePSousType(ArrayList<Sous_type_Prestation> listePSousType)
    {
        this.listePSousType = listePSousType;
    }


    public void addSousType(Sous_type_Prestation sst)
    {
        listePSousType.add(sst);

    }

    public void removeSousType(Sous_type_Prestation sst)
    {
        listePSousType.remove(sst);
    }

}
