package job;

import javax.persistence.*;
import java.util.ArrayList;

@Entity
@Table(name = "T_VILLE", schema = "dbo", catalog = "PROGICA")
public class Ville
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */




    private String numInsee;
    private String nomVille;
    private Double longitude;
    private Double latitude;
    private String codePostal;

    @Id
    @Column(name = "VLE_NUM_INSEE")
    public String getNumInsee()
    {
        return numInsee;
    }

    public void setNumInsee(String vleNumInsee)
    {
        this.numInsee = vleNumInsee;
    }

    @Basic
    @Column(name = "VLE_NOM_VILLE")
    public String getNomVille()
    {
        return nomVille;
    }

    public void setNomVille(String vleNomVille)
    {
        this.nomVille = vleNomVille;
    }

    @Basic
    @Column(name = "VLE_LONGITUDE")
    public Double getLongitude()
    {
        return longitude;
    }

    public void setLongitude(Double vleLongitude)
    {
        this.longitude = vleLongitude;
    }

    @Basic
    @Column(name = "VLE_LATITUDE")
    public Double getLatitude()
    {
        return latitude;
    }

    public void setLatitude(Double vleLatitude)
    {
        this.latitude = vleLatitude;
    }

    @Basic
    @Column(name = "VLE_CODE_POSTAL")
    public String getCodePostal()
    {
        return codePostal;
    }

    public void setCodePostal(String vleCodePostal)
    {
        this.codePostal = vleCodePostal;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Ville that = (Ville) o;

        if (numInsee != null ? !numInsee.equals(that.numInsee) : that.numInsee != null)
        {
            return false;
        }
        if (nomVille != null ? !nomVille.equals(that.nomVille) : that.nomVille != null)
        {
            return false;
        }
        if (longitude != null ? !longitude.equals(that.longitude) : that.longitude != null)
        {
            return false;
        }
        if (latitude != null ? !latitude.equals(that.latitude) : that.latitude != null)
        {
            return false;
        }
        if (codePostal != null ? !codePostal.equals(that.codePostal) : that.codePostal != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = numInsee != null ? numInsee.hashCode() : 0;
        result = 31 * result + (nomVille != null ? nomVille.hashCode() : 0);
        result = 31 * result + (longitude != null ? longitude.hashCode() : 0);
        result = 31 * result + (latitude != null ? latitude.hashCode() : 0);
        result = 31 * result + (codePostal != null ? codePostal.hashCode() : 0);
        return result;
    }
}
