package job;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "T_PROPOSER_PRESTATION")
public class Prestation_Gite
{

    /**
     * @author IOOSSEN AURELIEN created on 29/08/2019
     */

    
    private Prestation prestation;
    private int prix_prestation;

    public Prestation_Gite(Prestation prestation, int prix_prestation)
    {
        this.prestation = prestation;

        this.prix_prestation = prix_prestation;
    }


    public Prestation getPrestation()
    {
        return prestation;
    }

    public void setPrestation(Prestation prestation)
    {
        this.prestation = prestation;
    }


    public int getPrix_prestation()
    {
        return prix_prestation;
    }

    public void setPrix_prestation(int prix_prestation)
    {
        this.prix_prestation = prix_prestation;
    }

    @Override
    public String toString()
    {
        return "Prestation_Gite{" + "prestation=" + prestation + ", prix_prestation=" + prix_prestation + '}';
    }
}
