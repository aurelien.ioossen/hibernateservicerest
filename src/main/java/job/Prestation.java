package job;

import javax.persistence.Transient;
import java.util.ArrayList;

public class Prestation
{

    /**
     *  @author IOOSSEN AURELIEN created on 27/08/2019 
     *
     */

    private int id_prestation ;
    private String libelle_prestation;
    @Transient
    private ArrayList<Gite> listeGite;
    private Sous_type_Prestation sstype_prestation;

    public Prestation(int id_prestation, String libelle_prestation)
    {
        this.id_prestation = id_prestation;
        this.libelle_prestation = libelle_prestation;
    }


    public int getId_prestation()
    {
        return id_prestation;
    }

    public void setId_prestation(int id_prestation)
    {
        this.id_prestation = id_prestation;
    }

    public String getLibelle_prestation()
    {
        return libelle_prestation;
    }

    public void setLibelle_prestation(String libelle_prestation)
    {
        this.libelle_prestation = libelle_prestation;
    }

    public ArrayList<Gite> getListeGite()
    {
        return listeGite;
    }

    public void setListeGite(ArrayList<Gite> listeGite)
    {
        this.listeGite = listeGite;
    }

    public void setSousTypePrestation (Sous_type_Prestation sstype_prestation){

        this.sstype_prestation = sstype_prestation;
    }


    @Override
    public String toString()
    {
        return libelle_prestation ;
    }
}
