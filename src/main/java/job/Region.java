package job;

import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "T_REGION", schema = "dbo", catalog = "PROGICA", uniqueConstraints = @UniqueConstraint(columnNames = "RGN_CODE"))
public class Region
{

    /**
     * @author IOOSSEN AURELIEN created on 04/11/2019


     */

    private String RGN_code;
    private String RGN_nom;
    private List<Departement> listDepartement;

    public Region(String RGN_code, String RGN_nom)
    {
        this.RGN_code = RGN_code;
        this.RGN_nom = RGN_nom;
        listDepartement = new ArrayList<>();
    }

    public Region()
    {

    }

    @Id
    @Column(name = "RGN_CODE")
    public String getRGN_code()
    {
        return RGN_code;
    }

    @Column(name = "RGN_NOM")
    public String getRGN_nom()
    {
        return RGN_nom;
    }

    public void setRGN_code(String RGN_code)
    {
        this.RGN_code = RGN_code;
    }

    public void setRGN_nom(String RGN_nom)
    {
        this.RGN_nom = RGN_nom;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Region that = (Region) o;

        if (RGN_code != null ? !RGN_code.equals(that.RGN_code) : that.RGN_code != null)
        {
            return false;
        }
        if (RGN_nom != null ? !RGN_nom.equals(that.RGN_nom) : that.RGN_nom != null)
        {
            return false;
        }

        return true;
    }


    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "DPT_REGION")

    public List<Departement> getListDepartement()
    {
        return listDepartement;
    }

    public void setListDepartement(List<Departement> listDepartement)
    {
        this.listDepartement = listDepartement;
    }

    @Override
    public int hashCode()
    {
        int result = RGN_code != null ? RGN_code.hashCode() : 0;
        result = 31 * result + (RGN_nom != null ? RGN_nom.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        return "Region{" + "RGN_code='" + RGN_code + '\'' + ", RGN_nom='" + RGN_nom + '\'' + ", listDepartement=" + '}';
    }
}
