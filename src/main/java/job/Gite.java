package job;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;

@Entity
@Table(name = "T_GITE", schema = "dbo", catalog = "PROGICA")
public class Gite
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */





    private int idGite;
    private String libelleGite;
    private int surfaceHabitable;
    private int nmbCouchage;
    private int nmbChambre;
    private BigDecimal tarif;
    private String complementAdresse;
    private String complementGeo;
    private String numero;
    private String rue;
    private String lieuDit;
    private Ville ville;
    private Personne proprietaire;
    private Personne contact;
    private ArrayList<Prestation_Gite> listePrestation;




    @Id
    @Column(name = "GITE_ID")
    public int getIdGite()
    {
        return idGite;
    }

    public void setIdGite(int giteId)
    {
        this.idGite = giteId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GITE_NUM_INSEE")
    public Ville getVille()
    {
        return ville;
    }

    public void setVille(Ville ville)
    {
        this.ville = ville;
    }


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GITE_ID_PROPRIETAIRE")
    public Personne getProprietaire()
    {
        return proprietaire;
    }

    public void setProprietaire(Personne proprietaire)
    {
        this.proprietaire = proprietaire;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "GITE_ID_CONTACT")
    public Personne getContact()
    {
        return contact;
    }

    public void setContact(Personne contact)
    {
        this.contact = contact;
    }



    @Basic
    @Column(name = "GITE_NOM")
    public String getLibelleGite()
    {
        return libelleGite;
    }

    public void setLibelleGite(String giteNom)
    {
        this.libelleGite = giteNom;
    }

    @Basic
    @Column(name = "GITE_SURFACE")
    public int getSurfaceHabitable()
    {
        return surfaceHabitable;
    }

    public void setSurfaceHabitable(int giteSurface)
    {
        this.surfaceHabitable = giteSurface;
    }

    @Basic
    @Column(name = "GITE_NMBRE_COUCHAGE")
    public int getNmbCouchage()
    {
        return nmbCouchage;
    }

    public void setNmbCouchage(int giteNmbreCouchage)
    {
        this.nmbCouchage = giteNmbreCouchage;
    }

    @Basic
    @Column(name = "GITE_NMBRE_CHAMBRE")
    public int getNmbChambre()
    {
        return nmbChambre;
    }

    public void setNmbChambre(int giteNmbreChambre)
    {
        this.nmbChambre = giteNmbreChambre;
    }

    @Basic
    @Column(name = "GITE_TARIF_PROPRIETAIRE")
    public BigDecimal getTarif()
    {
        return tarif;
    }

    public void setTarif(BigDecimal giteTarifProprietaire)
    {
        this.tarif = giteTarifProprietaire;
    }

    @Basic
    @Column(name = "GITE_COMPLEMENT_ADRESSE")
    public String getComplementAdresse()
    {
        return complementAdresse;
    }

    public void setComplementAdresse(String giteComplementAdresse)
    {
        this.complementAdresse = giteComplementAdresse;
    }

    @Basic
    @Column(name = "GITE_COMPLEMENT_GEOGRAPHIQUE")
    public String getComplementGeo()
    {
        return complementGeo;
    }

    public void setComplementGeo(String giteComplementGeographique)
    {
        this.complementGeo = giteComplementGeographique;
    }

    @Basic
    @Column(name = "GITE_NUMERO")
    public String getNumero()
    {
        return numero;
    }

    public void setNumero(String giteNumero)
    {
        this.numero = giteNumero;
    }

    @Basic
    @Column(name = "GITE_LIBELLE_RUE")
    public String getRue()
    {
        return rue;
    }

    public void setRue(String giteLibelleRue)
    {
        this.rue = giteLibelleRue;
    }

    @Basic
    @Column(name = "GITE_LIEU_DIT")
    public String getLieuDit()
    {
        return lieuDit;
    }

    public void setLieuDit(String giteLieuDit)
    {
        this.lieuDit = giteLieuDit;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Gite that = (Gite) o;

        if (idGite != that.idGite)
        {
            return false;
        }
        if (surfaceHabitable != that.surfaceHabitable)
        {
            return false;
        }
        if (nmbCouchage != that.nmbCouchage)
        {
            return false;
        }
        if (nmbChambre != that.nmbChambre)
        {
            return false;
        }
        if (libelleGite != null ? !libelleGite.equals(that.libelleGite) : that.libelleGite != null)
        {
            return false;
        }
        if (tarif != null ? !tarif.equals(that.tarif) : that.tarif != null)
        {
            return false;
        }
        if (complementAdresse != null ? !complementAdresse.equals(that.complementAdresse) : that.complementAdresse != null)
        {
            return false;
        }
        if (complementGeo != null ? !complementGeo.equals(that.complementGeo) : that.complementGeo != null)
        {
            return false;
        }
        if (numero != null ? !numero.equals(that.numero) : that.numero != null)
        {
            return false;
        }
        if (rue != null ? !rue.equals(that.rue) : that.rue != null)
        {
            return false;
        }
        if (lieuDit != null ? !lieuDit.equals(that.lieuDit) : that.lieuDit != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = idGite;
        result = 31 * result + (libelleGite != null ? libelleGite.hashCode() : 0);
        result = 31 * result + surfaceHabitable;
        result = 31 * result + nmbCouchage;
        result = 31 * result + nmbChambre;
        result = 31 * result + (tarif != null ? tarif.hashCode() : 0);
        result = 31 * result + (complementAdresse != null ? complementAdresse.hashCode() : 0);
        result = 31 * result + (complementGeo != null ? complementGeo.hashCode() : 0);
        result = 31 * result + (numero != null ? numero.hashCode() : 0);
        result = 31 * result + (rue != null ? rue.hashCode() : 0);
        result = 31 * result + (lieuDit != null ? lieuDit.hashCode() : 0);
        return result;
    }
}
