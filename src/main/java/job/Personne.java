package job;

import javax.persistence.*;

@Entity
@Table(name = "T_PERSONNE", schema = "dbo", catalog = "PROGICA")
public class Personne
{

    /**
     *  @author IOOSSEN AURELIEN created on 04/11/2019 
     *
     */



    private int id_personne;
    private String nom_personne;
    private String num_tph;
    private String mail;

    @Id
    @Column(name = "PRS_ID")
    public int getId_personne()
    {
        return id_personne;
    }

    public void setId_personne(int prsId)
    {
        this.id_personne = prsId;
    }

    @Basic
    @Column(name = "PRS_NOM")
    public String getNom_personne()
    {
        return nom_personne;
    }

    public void setNom_personne(String prsNom)
    {
        this.nom_personne = prsNom;
    }

    @Basic
    @Column(name = "PRS_TPH")
    public String getNum_tph()
    {
        return num_tph;
    }

    public void setNum_tph(String prsTph)
    {
        this.num_tph = prsTph;
    }

    @Basic
    @Column(name = "PRS_MAIL")
    public String getMail()
    {
        return mail;
    }

    public void setMail(String prsMail)
    {
        this.mail = prsMail;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Personne that = (Personne) o;

        if (id_personne != that.id_personne)
        {
            return false;
        }
        if (nom_personne != null ? !nom_personne.equals(that.nom_personne) : that.nom_personne != null)
        {
            return false;
        }
        if (num_tph != null ? !num_tph.equals(that.num_tph) : that.num_tph != null)
        {
            return false;
        }
        if (mail != null ? !mail.equals(that.mail) : that.mail != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = id_personne;
        result = 31 * result + (nom_personne != null ? nom_personne.hashCode() : 0);
        result = 31 * result + (num_tph != null ? num_tph.hashCode() : 0);
        result = 31 * result + (mail != null ? mail.hashCode() : 0);
        return result;
    }

    @Override
    public String toString()
    {
        return "Personne{" + "prsId=" + id_personne + ", prsNom='" + nom_personne + '\'' + ", prsTph='" + num_tph + '\'' + ", prsMail='" + mail + '\'' + '}';
    }
}
